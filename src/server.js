"use strict";
exports.__esModule = true;
var express_1 = require("express");
var server = /** @class */ (function () {
    function server() {
        this.app = express_1;
    }
    server.prototype.config = function () {
        this.app.set('port', 3000);
    };
    server.prototype.routes = function () {
    };
    server.prototype.start = function () {
        var _this = this;
        this.app.listen(this.app.get('port'), function () {
            console.log('Corriendo en el puerto' + _this.app.getPort());
        });
    };
    return server;
}());
var servidor = new server();
servidor.start();
