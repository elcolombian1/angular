import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import {Routes,RouterModule} from '@angular/router';

import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule,MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule, MatCardModule, MatCheckboxModule } from '@angular/material';

import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { EmailComponent } from './email/email.component';
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';


import { GoogleComponent } from './google/google.component';
import { SmsComponent } from './sms/sms.component';

const firebaseConfig = {
  apiKey: "AIzaSyDtL7IwKNZUtXU7aMs2UDqhPto40CEG_54",
  authDomain: "apps-270723.firebaseapp.com",
  databaseURL: "https://apps-270723.firebaseio.com",
  projectId: "apps-270723",
  storageBucket: "apps-270723.appspot.com",
  messagingSenderId: "521903976438",
  appId: "1:521903976438:web:3fac093bbf070b98a961d7",
  measurementId: "G-2SPR4J0664"
};

const routes:Routes = [
  {path:'',component:GoogleComponent},
  {path:'login/google',component:GoogleComponent},
  {path:'login/email',component:EmailComponent},
  {path:'login/sms',component:SmsComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    EmailComponent,
    GoogleComponent,
    SmsComponent,
    
  ],
  imports: [
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatCheckboxModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
