import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import {AuthHttpService} from '../servicio/auth.http.service';
@Component({
  selector: 'app-google',
  templateUrl: './google.component.html',
  styleUrls: ['./google.component.css']
})
export class GoogleComponent implements OnInit {
  check:boolean;
  constructor(public auth: AuthService,public http:AuthHttpService) {

   }

  ngOnInit() {
  }
  httpTest(){
    this.http.get_test();
  }
}
