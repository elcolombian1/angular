import { Component, OnInit } from '@angular/core';
import { WindowService } from '../servicio/window.service';;
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import {AuthHttpService} from '../servicio/auth.http.service';

@Component({
  selector: 'app-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.css'],
  providers:[WindowService],
})
export class SmsComponent implements OnInit {

 
  windowRef: any;

  verificationCode: string;

  user: any;

  phoneNumber:string;

  constructor(private win: WindowService,private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private auth:AuthHttpService) { }

  ngOnInit() {
    this.windowRef = this.win.windowRef
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container')

    this.windowRef.recaptchaVerifier.render()
  }


  sendLoginCode() {

    const appVerifier = this.windowRef.recaptchaVerifier;

    const num = this.phoneNumber;

    firebase.auth().signInWithPhoneNumber(num, appVerifier)
            .then(result => {

                this.windowRef.confirmationResult = result;

            })
            .catch( error => console.log(error) );

  }

  verifyLoginCode() {
    this.windowRef.confirmationResult
                  .confirm(this.verificationCode)
                  .then( result => {
                    this.user = result.user;
                    this.auth.firebase_auth(this.user);


    })
    .catch( error => console.log(error, "Incorrect code entered?"));
  }

  get_test(){
    this.auth.get_test();
  }



}
