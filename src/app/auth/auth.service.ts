import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {AuthHttpService} from '../servicio/auth.http.service';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';


@Injectable({ providedIn: 'root' })
export class AuthService {

    
    constructor(
        private afAuth: AngularFireAuth,
        private afs: AngularFirestore,
        private router: Router,
        private auth:AuthHttpService,
    ) { 
    }

    async googleSignin() {
      const provider = new auth.GoogleAuthProvider();
      const credential = await this.afAuth.auth.signInWithPopup(provider);
      this.auth.firebase_auth(credential.user);

   
    }
    async signOut() {
      await this.afAuth.auth.signOut();
      this.router.navigate(['/']);
    }


}