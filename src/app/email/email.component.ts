import { Component, OnInit } from '@angular/core';
import {Validators,FormBuilder,FormGroup} from '@angular/forms';
import {AuthHttpService} from '../servicio/auth.http.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css'],
  providers:[AuthHttpService]
})
export class EmailComponent implements OnInit {
  loginGroup:FormGroup;
  mensaje_error:String;
  constructor(private frombuilder:FormBuilder,private auth:AuthHttpService,private router:Router) { }

  ngOnInit() {
    this.loginGroup=this.frombuilder.group(
      {

        email:['',Validators.required,Validators.email ],
        pass:['',Validators.required],
        
        remember_me:['']
    }

    );
  }

  get_try(){
    this.auth.get_test();
  }
  validar(){
    const data={
      email:this.loginGroup.controls.email.value,
      password:this.loginGroup.controls.pass.value,
      rememeber_me:this.loginGroup.controls.remember_me.value,
      
    }
    this.auth.authentication(data).subscribe(data=>{
                console.log(data);
                localStorage.setItem('logedin','true');
                // this.router.navigate(['/']);           
        
        

    })
  }

}
