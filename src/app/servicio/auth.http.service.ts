import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders } from '@angular/common/http';

const headers = new HttpHeaders({
              'Content-Type':'application/json',
              'Accept':'application/json',
              });

@Injectable({
  providedIn: 'root'
})


export class AuthHttpService {
    url:string;
    constructor(private http:HttpClient) { 

      this.url="http://localhost:8000/api/v1/login"
    }

      firebase_auth(data){
        this.http.get('http://localhost:8000/sanctum/csrf-cookie',{headers:headers, withCredentials: true}).subscribe(data)
        {
          this.url="http://localhost:8000/api/v1/firebase";     
          console.log({token:data.xa});
          this.http.post(this.url,{token:data.xa},{headers:headers,withCredentials: true}).subscribe((data)=>{
            console.log(data);
          })
          
        }
      }
    
      get_test(){
       
       this.http.get('http://localhost:8000/api/v1/users',{headers:headers,withCredentials: true}).subscribe((data)=>{
        console.log(data);
       })    
      }
      authentication(data){
        this.http.get('http://localhost:8000/sanctum/csrf-cookie',{headers:headers, withCredentials: true}).subscribe(data)
        {
          this.url="http://localhost:8000/api/v1/login";     
          console.log(data)
          return(this.http.post(this.url,data,{headers:headers,withCredentials: true}));
        }    
      }

      registro(data)
      {
        return(this.http.post(this.url,data,{headers:headers}))
      }

}
