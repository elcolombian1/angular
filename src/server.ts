import express, {Application} from 'express';

class server{

	public app:Application; 
	
	constructor(){
		this.app=express();
	}

	config():void{
			this.app.set('port',3000);

	}
	routes():void{


	}

	start():void{
		this.app.listen(this.app.get('port'),()=>{
			console.log('Corriendo en el puerto' + this.app.getPort())

		})
	}
}

const servidor=new server();
servidor.start();